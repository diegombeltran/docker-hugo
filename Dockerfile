FROM docker:latest

ARG VCS_REF
ARG BUILD_DATE
MAINTAINER Diego M. Beltrán <yo@diegomunozbeltran.com>
ENV GOROOT /usr/lib/go
ENV GOPATH /gopath
ENV GOBIN /gopath/bin
ENV PATH $PATH:$GOROOT/bin:$GOPATH/bin

RUN apk update && apk add awscli musl-dev openssh sshpass curl git mercurial bzr go make && go get -v github.com/spf13/hugo && rm -rf $GOPATH/src/* && rm -rf $GOPATH/pkg/* && apk del go curl mercurial bzr
LABEL   org.label-schema.build-date=$BUILD_DATE \
        org.label-schema.vcs-ref=$VCS_REF\
        org.label-schema.vcs-url="https://gitlab.com/diegombeltran/docker-hugo"

